/**
 * FLickr Galleria
 */

(function($) {
  Drupal.behaviors.flickr_galleria = {
    attach: function(context, settings) {
      
      if(typeof Drupal.settings.flickr_galleria_items != 'undefined') {

        // Wait until any animation is truly over.
        var t=setTimeout(function(){
          // Initialize Galleria
					
					for(var i in Drupal.settings.flickr_galleria_items) {
						for(var a in Drupal.settings.flickr_galleria_items[i]) {
						console.log(Drupal.settings.flickr_galleria_items[i][a])
						Galleria.run('#' + Drupal.settings.flickr_galleria_items[i][a].count, {
								flickr: 'set:' + Drupal.settings.flickr_galleria_items[i][a].id,
								height: parseInt(Drupal.settings.flickr_galleria_items[i][a].height),
								responsive: true
						});
						}
					}
        }, 1200);
      }
    }
  }
})(jQuery);