<?php

  /**
   * @file
   * Template for embedding Flickr slideshows for sets.
   * 
   * Variables available
   * $set_id.
	 * $container_id
   */
?>
<div id="<?php print $container_id ?>" class="flickr-slideshow-wrapper">
	
</div>